﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banking
{
    class Bank
    {
        private string _name;
        private string _address;
        private List<Client> _clientsList = new List<Client>();

        public Bank()
        {
            _name = "Unknown name";
            _address = "Unknown address";
        }

        public Bank(string name, string address)
        {
            _name = name;
            _address = address;
        }

        public void ShowBankInformation()
        {
            Console.WriteLine("Bank title: " + _name);
            Console.WriteLine("Bank address: " + _address);
            Console.WriteLine();
        }

        public void Add()
        {
            Console.WriteLine("Enter client information to add to the clients list: ");
            Console.Write("Surname: ");
            string surname = Console.ReadLine();
            Console.Write("Name: ");
            string name = Console.ReadLine();
            Console.Write("Age: ");
            uint age = (uint)Convert.ToInt32(Console.ReadLine());
            Console.Write("Balance: ");
            float balance = (float)Convert.ToDouble(Console.ReadLine());

            Client client = new Client(name, surname, age, balance);

            _clientsList.Add(client);
            Console.WriteLine($"Client № {_clientsList.Count} added to the { _name} clients list.");
            Console.WriteLine();
        }

        public void Remove()
        {
            bool isClientFinded = false;

            Console.WriteLine("Enter client surname to remove from the clients list.");
            ShowAllClientsSurnames();
            string surname = Console.ReadLine();

            foreach (Client client in _clientsList)
            {
                if (client.Surname == surname)
                {
                    Console.WriteLine($"Client №{(_clientsList.IndexOf(client) + 1)} removed from the {_name} clients list.");
                    Console.WriteLine();
                    _clientsList.Remove(client);
                    isClientFinded = true;
                    break;
                }
            }

            if (!isClientFinded)
            {
                Console.WriteLine("Client with this surname is not founded.");
                Console.WriteLine();
            }
        }

        public void ChangeBalance()
        {
            bool isClientFinded = false;
            float newBalance = 0f;

            Console.WriteLine("Enter client surname to change balance.");
            ShowAllClientsSurnames();
            string surname = Console.ReadLine();
            Console.Write("Enter client changed balance: ");
            newBalance = (float)Convert.ToDouble(Console.ReadLine());

            foreach (Client client in _clientsList)
            {
                if (client.Surname == surname)
                {
                    client.ChangeClientBalance(newBalance);
                    Console.WriteLine();
                    isClientFinded = true;
                    break;
                }
            }

            if (!isClientFinded)
            {
                Console.WriteLine("Client with this surname is not founded.");
                Console.WriteLine();
            }
        }

        public void ShowClient()
        {
            bool isClientFinded = false;

            Console.Write("Enter client surname to show information: ");
            ShowAllClientsSurnames();
            string surname = Console.ReadLine();

            foreach (Client client in _clientsList)
            {
                if (client.Surname == surname)
                {
                    client.ShowClientInformation();
                    Console.WriteLine();
                    isClientFinded = true;
                    break;
                }
            }

            if (!isClientFinded)
            {
                Console.WriteLine("Client with this surname is not founded.");
                Console.WriteLine();
            }
        }

        public void ShowAllClients()
        {
            SurnameSortingClients();

            Console.WriteLine($"All clients of the {_name}:");
            foreach (Client client in _clientsList)
            {
                Console.WriteLine("№" + (_clientsList.IndexOf(client) + 1) + ".");
                client.ShowClientInformation();
                Console.WriteLine();
            }
        }

        public void ShowAllClientsSurnames()
        {
            SurnameSortingClients();

            Console.WriteLine("All clients:");
            foreach (Client client in _clientsList)
            {
                Console.WriteLine($"№{(_clientsList.IndexOf(client) + 1)}. {client.Surname} (balance: {client.Balance}).");
            }
            Console.WriteLine();
        }

        private void SurnameSortingClients()
        {
            Client temp;
            for (int i = 0; i < _clientsList.Count - 1; i++)
            {
                for (int j = i + 1; j < _clientsList.Count; j++)
                {
                    if (_clientsList[i].Surname[0] > _clientsList[j].Surname[0])
                    {
                        temp = _clientsList[i];
                        _clientsList[i] = _clientsList[j];
                        _clientsList[j] = temp;
                    }
                    else if (_clientsList[i].Surname[0] == _clientsList[j].Surname[0])
                    {
                        if (_clientsList[i].Surname[1] > _clientsList[j].Surname[1])
                        {
                            temp = _clientsList[i];
                            _clientsList[i] = _clientsList[j];
                            _clientsList[j] = temp;
                        }
                        else if (_clientsList[i].Surname[1] == _clientsList[j].Surname[1])
                        {
                            if (_clientsList[i].Surname[2] > _clientsList[j].Surname[2])
                            {
                                temp = _clientsList[i];
                                _clientsList[i] = _clientsList[j];
                                _clientsList[j] = temp;
                            }
                        }
                    }
                }
            }
        }
    }
}
