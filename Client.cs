﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banking
{
    class Client
    {
        private string _name;
        private string _surname;
        private uint _age;
        private float _balance;

        public Client()
        {
            _name = "Name";
            _surname = "Surname";
            _age = 1;
            _balance = 0f;
        }

        public Client(string name, string surname, uint age, float balance)
        {
            _name = name;
            _surname = surname;
            _age = age;
            _balance = balance;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Surname
        {
            get
            {
                return _surname;
            }
            set
            {
                _surname = value;
            }
        }

        public float Balance
        {
            get
            {
                return _balance;
            }
            set
            {
                _balance = value;
            }
        }

        public bool ChangeClientInformation(uint newAge)
        {
            if ((newAge > 0) && (newAge < 120))
            {
                _age = newAge;
                return true;
            }
            else
                return false;
        }

        public void ChangeClientInformation(string newSurname) => _surname = newSurname;
        public void ChangeClientInformation(string newName, string newSurname)
        {
            _name = newName;
            _surname = newSurname;
        }

        public void ChangeClientBalance(float newBalance) => _balance = newBalance;
        public void IncreaseClientBalance(float addingBalance) => _balance += addingBalance;
        public void DecreaseClientBalance(float lesseningBalance) => _balance -= lesseningBalance;

        public void ShowClientInformation()
        {
            Console.WriteLine("Surname: " + _surname);
            Console.WriteLine("Name: " + _name);
            Console.WriteLine("Age: " + _age);
            Console.WriteLine("Balance: " + _balance);
        }
    }
}