﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Banking;

namespace Program
{
    class Program
    {
        public static void Main()
        {
            Bank sberbank = new Bank("Sberbank", "Leninskaya st. 10");

            // Отображение информации о банке
            sberbank.ShowBankInformation();

            // Добавление четырех новых клиентов
            sberbank.Add();
            sberbank.Add();
            sberbank.Add();
            sberbank.Add();

            // Отображение полной информации о всех клиентах
            Console.Clear();
            sberbank.ShowAllClients();
            Console.ReadKey();
            Console.Clear();

            // Удаление одного клиента
            sberbank.Remove();

            // Добавление одного клиента
            sberbank.Add();

            // Изменение баланса двух клиентов
            sberbank.ChangeBalance();
            sberbank.ChangeBalance();

            // Удаление одного клиента
            sberbank.Remove();

            // Отображение полной информации о всех клиентах
            Console.Clear();
            sberbank.ShowAllClients();
            Console.ReadKey();
        }
    }
}
